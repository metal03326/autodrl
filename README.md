# AutoDRL

AutoDRL is a very flexible module to control switching between Daytime Running Lights and Low Beam Headlights based on ambient light. An added bonus is Coming/Leaving Home function.

#### Main features:
- Adjustable automatic switch between DRL and headlights
- Adjustable engine running detection
- Adjustable Coming/Leaving Home function
- Service procedure that allows adjusting of all parameters **without** the need of a computer

## Supported cars

Module should work with all cars that control headlights by supplying GND to a relay that will activate the headlights. I've installed and tested the module on the following vehicles:

- 2001 Mazda 626 GF
- 2013 Mazda 2 DE

#### Installation video (on both Mazda 626 GF and Mazda 2 DE)

[![](http://img.youtube.com/vi/Ii6k4HBw_I8/0.jpg)](https://www.youtube.com/watch?v=Ii6k4HBw_I8 "")

https://www.youtube.com/embed/u-u-nPaqKtM

#### Notes on installation
It turns out there is a hard to spot difference between headlight switches of the Mazda 626/6 and Mazda 2. The way I connected the Mazda 2 on that video is **wrong** - it works on first sight, but High Beams will not work while Auto is active. Mazda 626 does not have this issue. Here is the difference:
![](schematics/Mazda%206%20GG%20Headlight%20Switch.png)
![](schematics/Mazda%20626%20GF%20Headlight%20Switch.png)
Mazda 6 headlight switch (used in my 626) schematics as well as 626 switch schematics - note there is hard ground connection to the LO/HI pole. This is good, that is why when driver presses the High beams it works.
![](schematics/Mazda%202%20DE%20Headlight%20Switch.png)
On the Mazda 2 headlight switch, you can see there is **no** hard connection between Ground and LO/HI pole. In the video, I connected AutoDRL to pass GND to **V/W** cable, which is good for Low Beams only. But when the driver presses the switch for High Beams, there is no ground to go towards the High Beams relay. **Fortunately, the fix is easy** - instead of using the **V/W** cable, use the **B/W** cable. That will supply GND to the pole, effectively making the switch like Mazda 626/6 and allowing the driver to switch from Low Beams to High Beams.
## Hardware

The project makes use of **Arduino Pro Mini**, but you could use any other **5V** Arduino that has at least two **External Interrupts** and enough **Digital I/O** pins (check **Schematics** below). **5V** is required due to the voltage dividers on the hat - if you adjust them, you can use **3.3V**, code automatically will adjust.

### Power consumption

Module is made to be constantly powered on. It goes to sleep if there is nothing to do (engine and headlights are off). Power consumption **depends on the efficiency** of the power regulator. The **Schematics** use **0mA**, but the Arduino used has highly inefficient built-in power regulator which draws **2.9mA** when the controller is asleep. This is fine by me, but you could add an external regulator and **remove** the build-in one to lower the consumption.<br/>**Note**: Prior to measuring consumption I've **removed** the power LED.

## Software

Source code is written with [Arduino IDE](https://www.arduino.cc/) 2.0.

Code has been split into separate files to simplify code management for different functions. File names are self-explanatory, but still:
- [AutoDRL.ino](src/AutoDRL/AutoDRL.ino) is the main file and code for switching between DRL and headlights lies here.
- [ComingHome.ino](src/AutoDRL/ComingHome.ino) contains the code related to Coming/Leaving Home function.
- [ServiceProcedure.ino](src/AutoDRL/ServiceProcedure.ino) contains the code for the service procedure where you adjust your parameters without the need of a computer.
- [Helpers.ino](src/AutoDRL/Helpers.ino) contains shared code between all other file and code that doesn't fit any of them.
- [Globals.h](src/AutoDRL/Globals.h) is a central place with all the definitions. **You will need to adjust few things initially here** (see **Installation** below)

Changes can be found in the [CHANGELOG.md](CHANGELOG.md).

## Schematics

![](schematics/AutoDRL.png)

| Item                | Type                     | Qty  | Purpose                                                  |
| :---                | :---                     | :--- | :---                                                     |
| Arduino Pro Mini 5V | Microcontroller          | 1    | The brain                                                |
| GL5506              | Light Dependent Resistor | 1    | Changes resistance based on ambient light                |
| AP2310GN-HF-3TR     | NMOS Transistor          | 3    | Controls relays by ground signal                         |
| P6KE33A             | Transil Diode            | 3    | Protects the circuit                                     |
| 1N5819              | Schottky Diode           | 4    | Over-voltage protection                                  |
| 1kΩ                 | Resistor                 | 1    | Creates voltage divider along with the LDR               |
| 5.1kΩ               | Resistor                 | 2    | Used in a voltage divider and as pull-up resistor      |
| 10kΩ                | Resistor                 | 6    | Used both in voltage dividers and transistors protection |
| 15kΩ                | Resistor                 | 2    | Used in voltage dividers                                 |

Schematics have been done with [Circuit Diagram](https://www.circuit-diagram.org/). Check [schematics](schematics) folder.

Odd letters seen on near some lines are the cable colors as seen in the video. I've followed the naming convention found in all Mazda wiring diagram books and probably other manufacturers as well.

## Installation

Depending on what functions you want from the module, you'll need to trace few signals from the car you are going to install it to. Current hardware revision has 10 cables going out of the Arduino hat, enough for all the features. Signal names are written on the **Schematics**.

**Note**: Regardless of which features you'll choose, you'll need to adjust voltage divider values (**ignSignalResistor** and **ignGndResistor**) in [Globals.h](src/AutoDRL/Globals.h). If you do not do that, voltage reading will be off and module will not detect engine running properly. You can skip that step only if you are sure you'll **never** want to use engine running detection and will **always** use timer instead (Parameter 3, Engine running detection).

### With all features

Nothing special to do - implement the whole **Schematics** and play with parameters.

### Only with automatic DRL function

This way of installing the module is helpful not only if you do not like/need the function, but also if you have trouble getting the Unlock signal, or you just do not want to spend that time.

Disabling the Coming/Leaving Home, there are two approaches here:
- Do not touch and thing. This will leave **Coming Home** active, but you won't get any **Leaving Home** functionality since there is no Unlock signal. You can even set the **Coming Home** to **Manual** if you would like to, it'll work.
- Disable **Coming Home** as well. You can do that during installation (change **PARAM_7_DEFAULT** from **COMING_HOME_AUTO** to **COMING_HOME_NO** inside [Globals.h](src/AutoDRL/Globals.h)) or after that, in the **Service Procedure**. I strongly suggest you do that in the **Service Procedure**, do not change the default in the code, as this will change the blinking patterns, and they won't make sense to anyone else but you.

### Only with Manual Coming/Leaving Home function

This one will require the least amount of soldering and wiring, but it is tricky to implement. Keep in mind this will work **only** for Manual Coming/Leaving Home, **not** the automatic.<br/>**Note**: This is in theory possible, not tested.
1. Solder Auto input (D12) to ground. You can skip soldering related pull-up resistor (D11) as well. This way you won't deal with Auto switch and wiring, but you lose **Service Procedure** functionality.
2. Skip soldering of the DRL output transistor and all elements connected to it.
3. Skip soldering of the TNS output transistor and all elements connected to it.
4. Solder Vcc to A2 and do not solder A1. This will fake full brightness (so headlights never turn on automatically), and you won't need to deal with light sensor and wiring for it.
5. Adjust Coming Home parameters (7-10) inside [Globals.h](src/AutoDRL/Globals.h) since you won't have access to do that after installation. Set Manual mode, automatic will not function at all since the module will think it's very bright outside.

### Flashing the Arduino

In case you never did that, here are the steps:

1. Download [Arduino IDE](https://www.arduino.cc/).
2. Download the [src](src) folder.
3. Connect the programmer to the Arduino. If your programmer has each cable separate, this is the typical connection:
    - Arduino TX -> White cable
    - Arduino RX -> Green cable
    - Arduino Vcc -> Red cable
    - Arduino GND -> Black cable
4. Plug-in the programmer to the USB port of the computer. At that point you should see the Power LED get constantly lid. Also, it is normal to see the other built-in LED to start blinking as many Arduino boards come with Blink example pre-installed.
5. Install drivers for the programmer if needed. I cannot give you steps here, as it depends on the programmer and OS you are using.
6. Open [AutoDRL.ino](src/AutoDRL/AutoDRL.ino) with Arduino IDE.
7. From the Tools -> Boards menu select **Arduino Pro or Pro Mini** (or your model, if you are not using this one).
8. From the Tools -> Port menu select the port of your programmer (lookup Google how to find it, as it is OS dependant).
9. From the Tools -> Processor menu make sure **5V** processor is selected.
10. From the Tools -> Programmer menu select your programmer.
11. Adjust voltage divider values (**ignSignalResistor** and **ignGndResistor**) in [Globals.h](src/AutoDRL/Globals.h)
12. Flash by pressing the Upload button on the toolbar or Sketch -> Upload. **Note**: If your programmer has only 4 wires, then you'll need to press Reset button on the Arduino board as soon as you press Upload.

## Service procedure
This is a way to test all inputs and outputs as well as to adjust few parameters used by in different calculations.

### How to Enter
1. **Ignition** and **Auto** switch should be off
2. Turn on **ignition** and within **0.5** seconds turn it back off.
3. Within **5** seconds turn on **Auto** switch and within **0.5** seconds turn it back off.
4. Repeat **2** more times step **2** and **3** within **5** seconds between repetition.

If you've done it properly, car should respond by turning on TNS for a short interval of time. After that it'll wait for **7** seconds (with the idea for you to go out of the car to see IO check) after which IO check will start:
1. Turn on TNS for 2 seconds and then turn it off.
2. Wait 1 second
3. Turn on Low Beams for 2 seconds and then turn it off.
4. Wait 1 second
5. Turn on DRL for 2 seconds and then turn it off.
6. Wait for 7 seconds (with the idea for you to go back in the car).
7. Car will blink the firmware version: Long blinks are major version, short blinks are minor. For example version 1.2 will be LONG-SHORT-SHORT, 2.3 will be LONG-LONG-SHORT-SHORT-SHORT and 1.0 will be only LONG
8. Car will wait for 2 seconds
9. Car will blink one long time the TNS to inform you are in programming mode for Parameter 1
10. Car will wait for 2 seconds and then show current value for the Parameter 1. Check table in **Navigation between parameters** for blink pattern description.

### How to Exit
Turn on **Ignition** and **Auto** for **5** seconds. This will save all changes.

**WARNING**: If you leave the service procedure without any input for more that **2** minutes, the procedure will be stopped **DISCARDING** all changes.

## Navigation between parameters
Parameters use path-like activation (see image below for better understanding), meaning you cannot randomly activate parameters, you'll need to go **back** to entry point to activate another.

To change parameters you need to get to the parameter by tapping a specific key for that parameter from a specific entry point. Check table. Default entry point is Parameter 1.

Every time you change a parameter (see parameter entry points table below) car will do **n** amount of long blinks equal of the chosen parameter. Then it'll pause for **2** seconds. Then give you current value in the following patterns:

| Pattern                 | Meaning                       |
| :---                    | :---                          |
| **n** SHORT blinks      | **n** steps more than default |
| **n** LONG blinks       | **n** steps less than default |
| SHORT-LONG-SHORT blinks | Same as default               |

### Table visualisation of all parameter paths
Definition of tap: Less than 500ms<br/>
Definition of hold: Between 500ms and 1.5s

| Parameter | Action         | Entry point | Short description                   |
| :---      | :---           | :---        | :---                                |
| 1         | -              | -           | Intensity setting                   |
| 2         | Tap Ignition   | Parameter 1 | Intensity hysteresis percentage     |
| 3         | Hold Ignition  | Parameter 1 | Engine running detection            |
| 4         | Tap Ignition** | Parameter 3 | Engine start timer                  |
| 5         | Tap Ignition** | Parameter 3 | Engine start voltage                |
| 6         | Hold Ignition  | Parameter 5 | Engine start voltage hysteresis     |
| 7         | Unlock         | Parameter 1 | Coming/Leaving Home type            |
| 8         | Tap Ignition   | Parameter 7 | Coming/Leaving Home length          |
| 9         | Hold Ignition  | Parameter 7 | Manual Coming Home engine off timer |
| 10        | Tap Ignition   | Parameter 9 | Manual Leaving Home unlock timer    |
| 11        | Hold Ignition  | Parameter 2 | DRL on timer                        |

** Depends on the value of Parameter 3

Example 1: To go to Parameter 2 (Intensity hysteresis percentage) you need to tap ignition when inside of Parameter 1.<br/>
Example 2: To go to Parameter 8 (Coming home length) you need to first go to Parameter 7 (Coming home type) by pressing the unlock button when in Parameter 1 and then tapping ignition.

To exit a parameter simply do the same thing you did to go in that parameter. That will bring you back 1 step, so if you were in Parameter 2 or 3 you'll go back to Parameter 1, but if you were in Parameter 4 you'll go back to Parameter 3.

### Image visualisation of all parameter paths
![](diagrams/AutoDRL.png)

Created using [diagrams.net](https://app.diagrams.net/)

##Changing value of selected parameter
Auto switch does all the control regardless of the chosen parameter - one tap changes the value with 1 step.

You can change direction (default is increment) by holding the Auto for **2** seconds. Car will respond by 2 blinks in the following pattern:

| Pattern    | Meaning                                   |
| :---       | :---                                      |
| SHORT-LONG | You are going from increment to decrement |
| LONG-SHORT | You are going from decrement to increment |

Switching parameters does **NOT** reset direction.

## Parameters
**Step** is the amount **Default** is changed for each blink (short or long, see table in **Navigation between parameters**). **Min** and **Max** are boundaries. You will loop if you try to go outside them.

**Note**: All numbers (**Default**, **Min**, **Max** and **Step**) are adjustable at installation time, when flashing, but those should be more than enough. Still, you can tweak them if you want to.

| Parameter | Default | Min  | Max  | Step | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| :---      | :---    | :--- | :--- | :--- | :---                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 1         | 560     | 20   | 1020 | 20   | Intensity value. Sets when module turns on DRL and off headlights. Very dependent on the position of the LDR.<br/>**Note**: This parameter also adjusts automatic Coming/Leaving Home threshold.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 2         | 10      | 1    | 30   | 1    | Intensity hysteresis in **percents** of the intensity (**Parameter 1**). Use this to control lower threshold of the intensity band in which module switches between lights. **Parameter 1** sets when module will switch to DRL, while this parameter controls once on DRL, when it will go to headlights.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| 3         | 2       | 1    | 2    | 1    | Type of engine running detection.<br/>**1** means use time after **ignition** is turned **on** (adjust time in **Parameter 4**).<br/>**2** means smart detection based on charging voltage (adjust voltage band in **Parameter 5** and **6**)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| 4         | 10      | 0    | 30   | 1    | **Seconds** to wait after **ignition** is switched **on**, after which engine is considered running. Set to **0** if you would like lights to come on with **ignition** switch to **on**.<br>**Note**: This works only if **Parameter 3** is set to **1**.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| 5         | 13.3    | 10.0 | 15.0 | 0.1  | Charging voltage. This is used to detect if engine is running - if voltage is above this value, then engine is running. Set this to very low setting to get the module to activate when you turn the ignition to ON. Also, if you have issues with lights going crazy (on and off without reason), consider adjusting **Parameter 6**.<br/>**Note**: This works only if **Parameter 3** is set to **2**                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 6         | 10      | 1    | 30   | 1    | Charging voltage hysteresis in **percents** of the charging voltage (**Parameter 5**). Use this to control lower threshold of the voltage band in which module thinks engine is running. **Parameter 5** sets when lights will go on (engine is running), this one controls when they will go off (engine stopped running).<br/>**Use case 1**: Some cars (like Mazda 2 DE) have smarter utilization of the generator and it doesn't work all the time making lights come on and off at times. Setting this percentage to higher value makes voltage band thicker, fixing the issue.<br/>**Use case 2**: Turning on the air conditioner turns off the lights for few seconds. Again, increase this percentage if that happens to fix the issue.<br/>**Note**: This parameter works only if **Parameter 3** is set to **2** |
| 7         | 3       | 1    | 4    | 1    | Coming/Leaving Home type. See **Coming/Leaving Home types** for more info                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 8         | 30      | 10   | 120  | 10   | Coming/Leaving Home length in **seconds**. This adjusts for how long headlights are active for both **Coming** and **Leaving Home**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 9         | 10      | 5    | 60   | 5    | Manual Coming Home engine off timer. This adjusts how many **seconds** after engine is **off** driver can activate **Coming Home** by tapping **Auto** switch **off** then **on**.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| 10        | 5       | 1    | 10   | 1    | Manual Leaving Home unlock timer. This adjusts maximum seconds between Unlock signals that will count as **Leaving Home** activation. Example: If value is **2**, driver will need to unlock car twice within **2** seconds. If value is **3**, driver will need to unlock car twice within **3** seconds.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| 11        | 15      | 1    | 30   | 10   | Added in version **1.1** to combat flashing (fast switching between headlight and DRL) when going through some thick shadows. In version **1.0** this value used to be **0.5** seconds. Now it is adjustable and bumped up by a lot. Once Low Beams turn on, the driver will need to drive **this** amount of seconds above the threshold in order for the DRL to turn back on. Turning on Low Beams is still hard-coded at **0.5** seconds. This new value also eliminates the issue where Low Beams are being turned off too early while exiting a tunnel.                                                                                                                                                                                                                                                               |

### Coming/Leaving Home types
**Note**: Coming home works only if **Auto** switch is **on**, no matter of the type chosen.

| Value | Function                                                                                                                                                                                                                                                                                                                                                                                                              |
| :---  | :---                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 1     | No Coming/Leaving Home                                                                                                                                                                                                                                                                                                                                                                                                |
| 2     | Manual Coming/Leaving Home.<br/>**Coming Home** is activated by tapping **Auto** switch (**off** then **on**) **x** amount of seconds (adjusted in **Parameter 9**) after ignition is off.<br/>**Leaving Home** is activated by unlocking the car twice within **x** amount of seconds, **adjusted in Parameter 10**.<br/>**Note**: Both actions do **NOT** monitor ambient light. Control is left in driver's hands. |
| 3     | Automatic Coming/Leaving Home.<br/>**Coming Home** is activated only if it is dark outside and ignition has just been turned off.<br/>**Leaving Home** is activated when driver unlocks the car (one time) **and** it is dark outside (adjusted with **Parameter 1**).                                                                                                                                                |
| 4     | Automatic Coming Home with Manual Leaving Home.<br/>**Coming Home** works as described for value **3**.<br/>**Leaving Home** works as described for value **2**.<br/>**Use case**: You do not want to blind people in-front of the car when unlocking it.                                                                                                                                                             |

## To-do lists
### Software
- Add sharp light change detection
- Add artificial light detection
- Remove the need to wait for all blinks in the Service Procedure
  
### Hardware
- Add parking brake input

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
