# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres only to MAJOR.MINOR versioning scheme from [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1] - 2021-07-07

### Added

- Parameter 11 - DRL on timer.
- This CHANGELOG.md file.

### Fixed

- Flashing of headlights when going through thick shadows.
- Headlights turning off too early when exiting a tunnel.
- Lights blinking under certain conditions when engine running timer is used

### Changed

- Time needed for driver to drive above threshold to switch from Low Beams to DRL - from 0.5 to 15 seconds. It can be adjusted in the new Parameter 11.

## [1.0] - 2021-03-25

- Initial release

[1.1]: https://gitlab.com/metal03326/autodrl/-/tags/v1.1
[1.0]: https://gitlab.com/metal03326/autodrl/-/tags/v1.0