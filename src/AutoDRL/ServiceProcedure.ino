uint8_t serviceProcedureCycles;
uint8_t serviceProcedureParam;
int serviceProcedureDirection;

void feedbackBlink(byte times, int wait) {
  for (byte i = 0; i < times; i++) {
    setLights(LOW, HIGH, LOW);
    delay(wait);
    lightsOut();
    // 1000 was for showParam, but not value
    delay(500);    
  }  
}

void checkOutputs() {
  setLights(LOW, HIGH, LOW);

  delay(500);

  lightsOut();

  delay(7000);

  setLights(LOW, HIGH, LOW);

  delay(2000);

  lightsOut();

  delay(1000);

  setLights(LOW, LOW, HIGH);

  delay(2000);

  lightsOut();

  delay(1000);

  setLights(HIGH, LOW, LOW);

  delay(2000);

  lightsOut();
}

void showVersion() {
  const byte majorVersion = (byte)VER;
  const float minorVersion = VER - (float)majorVersion;

  #if defined(DEBUG)
    log("Major version:", (String)majorVersion);
    log("Minor version:", (String)minorVersion);
  #endif
  
  feedbackBlink(majorVersion, LONG);

  if (minorVersion > 0) {
    feedbackBlink((int)(minorVersion / 0.1), SHORT);    
  }
}

void showCurrentParamAndValue() {
  lightsOut();

  #if defined(DEBUG)
    log("Param " + (String)serviceProcedureParam + ":", (String)valueOf(serviceProcedureParam));
  #endif

  feedbackBlink(serviceProcedureParam, LONG);

  showCurrentValue();
}

void showCurrentValue() {
  // Wait a second to make sure driver has turned off all lights (Mazda 6 GG light switch has Auto after low beams, so to tap Auto driver needs to turn on TNS and low beams)
  delay(1000);
  
  const int currentParam = valueOf(serviceProcedureParam);
  const int defaultParam = params[serviceProcedureParam][PARAM_IDX_DEF];
  const int paramStep = params[serviceProcedureParam][PARAM_IDX_STEP];
  const int diff = currentParam - defaultParam;

  if (diff == 0) {
    feedbackBlink(1, SHORT);
    feedbackBlink(1, LONG);
    feedbackBlink(1, SHORT);
  } else {
    feedbackBlink(abs(diff) / paramStep, diff > 0 ? SHORT : LONG);
  }
}

void resetServiceProcedureVariables() {
  serviceProcedureCycles = 1;
  serviceProcedure = false;
  serviceProcedureTimer = 0;
  serviceProcedureParam = 1;
  serviceProcedureDirection = 1;
}

void enterServiceProcedure() {
  serviceProcedure = true;
  #if defined(DEBUG)
    log("Starting service procedure");
  #endif

  // Start timer to auto-exit service procedure if no input from the driver for specified period of time
  resetAbortTimeout();

  // Start blinking all the outputs
  checkOutputs();

  delay(7000);

  showVersion();

  delay(2000);

  // Show param 1 value
  showCurrentParamAndValue();
}

void exitServiceProcedure(bool save = false) {
  // Reset variables to default
  resetServiceProcedureVariables();

  #if defined(DEBUG)
    log("Exiting Service Procedure with saving:", (String)save);
  #endif

  // Acknowledge leaving the service procedure
  feedbackBlink(2, SHORT);

  if (save) {
    saveParams();
  } else {
    loadParams();
  }
}

void resetAbortTimeout() {
  serviceProcedureTimer = 120000 / interval;
}

void sp_ignition_trigger() {
  if (!serviceProcedure) {
    // Start countdown only if driver just stopped igintion and Auto switch is off
    if (serviceProcedureCycles == 1 && !ign && !isAutoOn()) {
      serviceProcedureTimer = 5000 / interval;
    }
  }
}

void sp_go_to_param(int param) {
  resetAbortTimeout();

  serviceProcedureParam = param;

  showCurrentParamAndValue();
}

void sp_loop() {
  // All params have ignition trigger either to enter next one or exit current one.
  // Also ignition is required to exit with saving all params
  if (ign) {
      //todo: Maybe use define to define tap interval so it is better for math afterwards
      delay(500);

      // Driver tapped ign and not on voltage hysteresis parameter (the only one that doesn't do anything on ignition tap)
      if (!digitalRead(i_ign)) {
        // Driver tapped in parameter that doesn't support tapping
        if (params[serviceProcedureParam][PARAM_IDX_IGN_TAP] == NULL) {
          // Special case for SPP_RUNNING_TYPE where we need current value to make a decision
          if (serviceProcedureParam == SPP_RUNNING_TYPE) {
            sp_go_to_param(valueOf(SPP_RUNNING_TYPE) == RUNNING_TIMER ? SPP_RUNNING_TIMER : SPP_RUNNING_VOLTAGE);
          } else {
            serviceProcedureTimer -= 500 / interval;
          }
        } else {
          sp_go_to_param(params[serviceProcedureParam][PARAM_IDX_IGN_TAP]);
        }
      }
      // If ignition is on for long, check if Auto is on as well - if so, wait for total of 5 seconds to exit the procedure
      else if (isAutoOn()) {
        delay(4500);

        // If both ignition and Auto are still on, exit and save
        if (digitalRead(i_ign) && isAutoOn()) {
          exitServiceProcedure(true);
        }
        else {
          serviceProcedureTimer -= 5000 / interval;
        }
      } else {
        delay(1000);

        // Check for ign hold
        if (!digitalRead(i_ign) && params[serviceProcedureParam][PARAM_IDX_IGN_HOLD] != NULL) {
          sp_go_to_param(params[serviceProcedureParam][PARAM_IDX_IGN_HOLD]);
        } else {
          serviceProcedureTimer -= 1500 / interval;
        }
      }
    }
    else if (digitalRead(i_unlock) && params[serviceProcedureParam][PARAM_IDX_UNLOCK] != NULL) {
      sp_go_to_param(params[serviceProcedureParam][PARAM_IDX_UNLOCK]);
    }
    else if (isAutoOn()) {
      delay(500);

      // Driver tapped Auto
      if (!isAutoOn()) {
        resetAbortTimeout();

        int newValue = valueOf(serviceProcedureParam) + params[serviceProcedureParam][PARAM_IDX_STEP] * serviceProcedureDirection;
        const int paramMin = params[serviceProcedureParam][PARAM_IDX_MIN];
        const int paramMax = params[serviceProcedureParam][PARAM_IDX_MAX];

        if (newValue < paramMin) {
          newValue = paramMax;
        } else if (newValue > paramMax) {
          newValue = paramMin;
        }

        params[serviceProcedureParam][PARAM_IDX_VALUE] = newValue;

        #if defined(DEBUG)
          log("Param " + (String)serviceProcedureParam + ":", (String)newValue);
        #endif
        
        showCurrentValue();
      } else {
        delay(1000);

        if (!isAutoOn()) {
          serviceProcedureDirection *= -1;

          // Blink to acknowledge
          feedbackBlink(1, serviceProcedureDirection > 0 ? LONG : SHORT);
          feedbackBlink(1, serviceProcedureDirection > 0 ? SHORT : LONG);
          
          resetAbortTimeout();
        } else {
          serviceProcedureTimer -= 1000 / interval;
        }
      }
    }
    else {
      serviceProcedureTimer--;
    }
  
    if (serviceProcedureTimer == 0) {
      exitServiceProcedure();
    }
}

void sp_timer_loop() {
  const bool even = serviceProcedureCycles % 2 == 0;
  const bool onCondition = even ? digitalRead(i_ign) : isAutoOn();
  
  #if defined(DEBUG)
    log("serviceProcedureCycles:", (String)serviceProcedureCycles);
  #endif

  // If driver tapped Ignition/Auto switch
  if (onCondition) {
    delay(500);

    // Ignition/Auto should first be returned to off otherwise we restart counters
    if (even ? !digitalRead(i_ign) : !isAutoOn()) {
      serviceProcedureCycles++;
      #if defined(DEBUG)
        log("Increasing serviceProcedureCycles to:", (String)serviceProcedureCycles);
      #endif

      if (serviceProcedureCycles == 6) {
        enterServiceProcedure();
      } else {
        #if defined(DEBUG)
          log("Waiting for more serviceProcedureCycles than current:", (String)serviceProcedureCycles);
        #endif
        serviceProcedureTimer = 5000 / interval;
      }
    }
    else {
      serviceProcedureCycles = 1;
      serviceProcedureTimer = 0;
    }
  } else {
    serviceProcedureTimer--;

    // Sleep if driver simply turned off the ignition and didn't do anything afterwards
    if (serviceProcedureTimer == 0 && comingHomeRemaingTime == 0 && manualComingHomeCountDown == 0) {
      sleep();
    }
  }
}
