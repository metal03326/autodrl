/*
  Automatic DRL with Coming Home function
*/
#include <EEPROM.h>
#include <avr/sleep.h>
#include "Globals.h"

// Used in calculations of the real voltage from the car
float finalVoltageMultiplier;

float calculatedIntesityLowerValue;
float calculatedVoltageUpperValue;
float calculatedVoltageLowerValue;
int calculatedIntensityDrlOnTimer;

uint8_t lightChangedCounter;
uint8_t voltageChangedCounter;
// This is used as a countdown for manual CH steps
int manualComingHomeCountDown;
// If this differs 0 Comming Home off timer will run
int comingHomeRemaingTime;
int engineIsRunningTimer;
bool prevIgn;
bool ign;

// Service procedure helper variables
bool serviceProcedure;
int serviceProcedureTimer;

int params[12][8] = {
  {NULL,  NULL,             NULL,             NULL,                  NULL, NULL,                                NULL,                         NULL                }, // Fictional array to occupy index 0 so we can match serviceProcedureParam and SPP_* to here without math.
//{VALUE, DEFAULT,          MIN,              MAX,                   STEP, ON_IGN_TAP,                          ON_IGN_HOLD,                  ON_UNLOCK           }, // Visual representation of the maeaning of all columns
  {NULL,  560,              20,               1020,                  20,   SPP_INTENSITY_HYSTERESIS,            SPP_RUNNING_TYPE,             SPP_COMING_HOME_TYPE}, // This is SPP_INTENSITY
  {NULL,  10,               1,                30,                    1,    SPP_INTENSITY,                       SPP_INTENSITY_DRL_ON_TIMER,   NULL                }, // This is SPP_INTENSITY_HYSTERESIS
  {NULL,  RUNNING_VOLTAGE,  RUNNING_TIMER,    RUNNING_VOLTAGE,       1,    NULL, /*special case in the code*/   SPP_INTENSITY,                NULL                }, // This is SPP_RUNNING_TYPE 
  {NULL,  10,               0,                30,                    1,    SPP_RUNNING_TYPE,                    NULL,                         NULL                }, // This is SPP_RUNNING_TIMER
  {NULL,  133,/*Will / 10*/ 100,/*Will / 10*/ 150, /*Will / 10*/     1,    SPP_RUNNING_TYPE,                    SPP_VOLTAGE_HYSTERESIS,       NULL                }, // This is SPP_RUNNING_VOLTAGE
  {NULL,  10,               1,                30,                    1,    NULL,                                SPP_RUNNING_VOLTAGE,          NULL                }, // This is SPP_VOLTAGE_HYSTERESIS
  {NULL,  COMING_HOME_AUTO, COMING_HOME_NO,   COMING_HOME_SEMI_AUTO, 1,    SPP_COMING_HOME_LENGTH,              SPP_COMING_HOME_MANUAL_TIMER, SPP_INTENSITY       }, // This is SPP_COMING_HOME_TYPE
  {NULL,  30,               10,               120,                   10,   SPP_COMING_HOME_TYPE,                NULL,                         NULL                }, // This is SPP_COMING_HOME_LENGTH
  {NULL,  10,               5,                60,                    5,    SPP_COMING_HOME_MANUAL_UNLOCK_TIMER, SPP_COMING_HOME_TYPE,         NULL                }, // This is SPP_COMING_HOME_MANUAL_TIMER
  {NULL,  5,                1,                10,                    1,    SPP_COMING_HOME_MANUAL_TIMER,        NULL,                         NULL                }, // This is SPP_COMING_HOME_MANUAL_UNLOCK_TIMER
  {NULL,  15,               1,                30,                    1,    NULL,                                SPP_INTENSITY_HYSTERESIS,     NULL                }  // This is SPP_INTENSITY_DRL_ON_TIMER
};

void setup() {
  pinMode(i_ign, INPUT);
  pinMode(i_unlock, INPUT);
  pinMode(o_tns, OUTPUT);
  pinMode(o_beams, OUTPUT);
  pinMode(o_auto_pull_up, OUTPUT);
  pinMode(o_ldr_pull_down, OUTPUT);
  pinMode(i_auto, INPUT);
  pinMode(o_drl, OUTPUT);

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);

  #if defined(DEBUG)
    Serial.begin(9600);
  
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
  #endif

  // Uncomment this to erase the whole EEPROM as if never touched
  // for (int i = 0; i < EEPROM.length(); i++) { EEPROM.write(i, 0xFF); }

  resetTimers();

  loadParams();

  attachInterrupt(digitalPinToInterrupt(i_unlock), onUnlock, CHANGE);
  attachInterrupt(digitalPinToInterrupt(i_ign), onIgnition, CHANGE);

  #if defined(FORCE_IGNITION)
    ign = HIGH;
  #else
    // Cover (most likely only testing) case when microcontroller powers on while ignition is on
    ign = digitalRead(i_ign);
  #endif

  finalVoltageMultiplier = readVcc() / 1024 * voltageMultiplier;

  resetServiceProcedureVariables();

  wakeupResistors();

  #if defined(FORCE_SERVICE_PROCEDURE)
    enterServiceProcedure();
  #endif
}

void onIgnition() {
  ign = digitalRead(i_ign);

  wakeupResistors();

  #if defined(DEBUG)
    log("Ignition trigger!");
  #endif

  sp_ignition_trigger();
}

bool shouldEngineStart(float voltage) {
  if (valueOf(SPP_RUNNING_TYPE) == RUNNING_TIMER) {
    engineIsRunningTimer -= interval;

    // This will always go negative because we are re-using logic for voltage change that checks multiple times
    return engineIsRunningTimer <= 0;
  }

  return voltage >= calculatedVoltageUpperValue;
}

void ignition() {
  if (isAutoOn()) {
    // Skip reading the analog pin if we know we are not going to be using the voltage anyhow. Just make sure voltage is bigger than calculatedVoltageLowerValue, to be on the safe side
    float voltage = calculatedVoltageLowerValue == 0 ? 1 : analogRead(i_volt) * finalVoltageMultiplier;

    // If CH is active, stop it and stop lights to supply more power for eventual engine start
    // This happens when user activates CH (manually or automatic) while unlocking the car, then turns on ignition before CH timer has run out.
    // Note: The "else" of the "if" for the voltage check will also turn off the lights (engine stalled edge case) which makes this check useless. However, if driver sets voltage param to lower value it is possible for the "else" to never be executed and the code in the "if" to get confused.
    if (comingHomeRemaingTime > 0) {
      ch_off();
    }

    // Now that we are sure CH is off, check if any lights are on - then the engine is running
    bool engineIsRunning = digitalRead(o_drl) || digitalRead(o_beams);

    // Engine is running
  #if defined(FORCE_ENGINE_RUNNING)
    if (true) {
  #else
    if (!engineIsRunning && shouldEngineStart(voltage)) {
  #endif
      voltageChangedCounter++;

      if (voltageChangedCounter > 5) {
        // This will execute once, as after execution engineIsRunning will become true
        engineRunning();
      }
    } else if (engineIsRunning && voltage < calculatedVoltageLowerValue) {
      // Keep measuring light input
      engineRunning();
    
      voltageChangedCounter++;
  
      // You stalled the engine, good job
      if (voltageChangedCounter > 5) {
        lightsOut();
      }
    } 
    // Keep measuring light input
    else if (engineIsRunning) {
      // In case there were one or more voltage readings below threshold but voltage recovered and engine never stopped, we need to reset the counter
      if (voltageChangedCounter != 0) {
        voltageChangedCounter = 0;
      }

      engineRunning();
    } else if (voltageChangedCounter != 0) {
      voltageChangedCounter = 0;
    }
  #if defined(DEBUG)
    else if (!engineIsRunning) {
      log("Voltage:", (String)voltage);
    }
  #endif
  }
  else {
    lightsOut();
  }
}

void engineRunning() {
  int light = analogRead(i_ldr);
  bool drlActive = digitalRead(o_drl);

  #if defined(DEBUG)
    log("Light:", (String)light);
  #endif

  // Nothing is on - engine just started
  if (!drlActive && !digitalRead(o_beams)) {
    if (light >= valueOf(SPP_INTENSITY)) {
      setLights(HIGH, LOW, LOW);
    } else {
      setLights(LOW, HIGH, HIGH);
    }
  } else if (drlActive && light < calculatedIntesityLowerValue) {
    lightChangedCounter++;

    if (lightChangedCounter > 5) {
      setLights(LOW, HIGH, HIGH);
    }
  } else if (!drlActive && light >= valueOf(SPP_INTENSITY)) {
    lightChangedCounter++;

    if (lightChangedCounter > calculatedIntensityDrlOnTimer) {
      setLights(HIGH, LOW, LOW);
    }
  } else if (lightChangedCounter != 0) {
    lightChangedCounter = 0;
  }
}

void loop() {
  if (serviceProcedure) {
    sp_loop();
  } else {
  
    // Engine is either running, about to run or stalled
    if (ign) {
      ignition();
    }
    else {
      // Reset engine running timer.
      //todo: This happens way too often. Find a better place
      engineIsRunningTimer = valueOf(SPP_RUNNING_TIMER) * 1000;

      ch_unlock_loop();
    
      // Driver just turned off ignition
      if (prevIgn && !ign) {
        // If we have CH, leave control over the (possible) ON lights to the CH
        if (valueOf(SPP_COMING_HOME_TYPE) != COMING_HOME_NO && isAutoOn()) {
          ch_ignitionOff_loop();
        }
        // Turn off lights when ignition has just been turned off.
        else {
          lightsOut();
        }
      }
      // Wait for the driver to activate CH (either by Auto switch or by another unlock)
      else if (manualComingHomeCountDown > 0) {
        ch_manualCountDown_loop();
      }
      // Check if CH is running and stop it after timeout, then go to sleep
      else if (comingHomeRemaingTime > 0) {
        ch_timeout_loop();
      }
      else if (serviceProcedureTimer == 0) {
        sleep();
      }
    }

    // We have at least 1 ignition signal that has come and we are about to either enter or exit service procedure
    if (serviceProcedureTimer > 0) {
      sp_timer_loop();
    }

    // Save previous ignition state in order to figure out when driver has turned off ignition (needed for coming home)
    prevIgn = ign;
  }

  delay(interval);
}
