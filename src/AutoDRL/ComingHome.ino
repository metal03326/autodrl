// Used to fight noisy unlock signal
bool lastUnlockValue;
unsigned long lastUnlockTime;

void onUnlock() {
  wakeupResistors();
  
  // Coming home works only if enadbled and Auto switch is On (LOW).
  // Also, ignore unlock when in service procedure, as it has different purpose there
  if (!serviceProcedure && valueOf(SPP_COMING_HOME_TYPE) != COMING_HOME_NO && isAutoOn()) {
    lastUnlockValue = digitalRead(i_unlock);
  }
}

void ch_unlock_loop() {
  // millis() usage is in order to fight fast HIGH-LOW-HIGH fluctuations during a real unlock signal - we need only 1 trigger when this happens and driver cannot unlock the car twice for 200ms, so we limit only 1 unlock signal per 200ms is allowed
  if (lastUnlockValue && (millis() - lastUnlockTime) > 200) {
    // Wait a bit and check again - this will filter out fast spikes (LOW-HIGH-LOW)
    delay(100);

    // If we are still on (lastUnlockValue is updating during the delay), then this really is unlock
    if (lastUnlockValue) {
      // Filter HIGH-LOW-HIGH fluctuations only if this is genuine unlock signal
      lastUnlockTime = millis();

      #if defined(DEBUG)
        log("Unlocked!");
      #endif
  
      const bool tnsActive = digitalRead(o_tns);
  
      // Prevent driver from activating Coming Home multiple times
      if (!digitalRead(o_beams)) {
        const int chValue = valueOf(SPP_COMING_HOME_TYPE);

        if (chValue == COMING_HOME_MANUAL || chValue == COMING_HOME_SEMI_AUTO) {
          #if defined(DEBUG)
            log("Manual CH");
          #endif
          // If previous timer has not run out, then we proceed with activation
          if (manualComingHomeCountDown > 0) {
            setLights(LOW, LOW, HIGH);
  
            // Reset for next time
            manualComingHomeCountDown = 0;
  
            // Start countdown
            comingHomeRemaingTime = valueOf(SPP_COMING_HOME_LENGTH) * 1000 / interval;
          }
          // Open window of x seconds for the driver to press again unlock to activate CH
          else {
            manualComingHomeCountDown = valueOf(SPP_COMING_HOME_MANUAL_UNLOCK_TIMER) * 1000 / interval;
          }
        }
        // We are automatic Coming Home and we need to check light before activation
        else if (analogRead(i_ldr) < valueOf(SPP_INTENSITY)) {
          #if defined(DEBUG)
            log("Auto CH!");
          #endif
          setLights(LOW, LOW, HIGH);
          // Start countdown
          comingHomeRemaingTime = valueOf(SPP_COMING_HOME_LENGTH) * 1000 / interval;
        }
      }
    }
  }
}

void ch_off() {
  comingHomeRemaingTime = 0;

  lightsOut();
}

void ch_ignitionOff_loop() {
  if (valueOf(SPP_COMING_HOME_TYPE) == COMING_HOME_MANUAL) {
    // Turn off lights while waiting. This is needed because of lights may have been ON when engine was running
    lightsOut();
    
    // Start/Reset the x seconds countdown for the driver to activate CH before we go to sleep
    manualComingHomeCountDown = valueOf(SPP_COMING_HOME_MANUAL_TIMER) * 1000 / interval;
  }
  // We are automatic CH. Do not check for current state of the beams as driver may have stalled the engine and then decided to just leave the car as is, turning off the ignition - beams will be off, but we still would like to get the coming home to activate if it is dark outside.
  else if (analogRead(i_ldr) < valueOf(SPP_INTENSITY)) {
    setLights(LOW, LOW, HIGH);

    // Start countdown
    comingHomeRemaingTime = valueOf(SPP_COMING_HOME_LENGTH) * 1000 / interval;
  }
  // Turn off lights if we are Auto and there is daylight outside. In theory lights at that point should be OFF already, but just to be on the safe side, turn them off
  else {
    lightsOut();
  }
}

void ch_manualCountDown_loop() {
  // If we get HIGH this means driver has switched Auto to off, which is the signal we expect to turn on the CH
  if (!isAutoOn()) {
    setLights(LOW, LOW, HIGH);

    // Start countdown
    comingHomeRemaingTime = valueOf(SPP_COMING_HOME_LENGTH) * 1000 / interval;

    // Reset for next time
    manualComingHomeCountDown = 0;
  }
  // Keep waiting
  else {
    manualComingHomeCountDown--;

    // Waiting is over - go to sleep
    if (manualComingHomeCountDown == 0) {
      sleep();
    }
  }
}

void ch_timeout_loop() {
  comingHomeRemaingTime--;
  
  if (comingHomeRemaingTime == 0) {
    sleep();
  }
}
