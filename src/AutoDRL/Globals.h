#ifndef GLOBALS
#define GLOBALS

#define VER 1.1
// Uncomment this for debug mode
// #define DEBUG 1
// #define FORCE_ENGINE_RUNNING 1
// #define FORCE_IGNITION 1
// #define FORCE_SERVICE_PROCEDURE 1

#define i_ign 2
#define i_unlock 3
#define i_auto 12
#define i_ldr A2
#define i_volt A3
#define o_tns 4
#define o_beams 5
// Optimize power usage by connecting the pullup resistor for Auto switch not to constant power, but to a digital pin
#define o_auto_pull_up 11
// Same goes for LDR's resistor
#define o_ldr_pull_down A1
#define o_drl 13

#define COMING_HOME_NO 1
#define COMING_HOME_MANUAL 2
#define COMING_HOME_AUTO 3
#define COMING_HOME_SEMI_AUTO 4
#define RUNNING_TIMER 1
#define RUNNING_VOLTAGE 2

// SPP = Service Procedure Parameter
#define SPP_INTENSITY 1
#define SPP_INTENSITY_HYSTERESIS 2
#define SPP_RUNNING_TYPE 3
#define SPP_RUNNING_TIMER 4
#define SPP_RUNNING_VOLTAGE 5
#define SPP_VOLTAGE_HYSTERESIS 6
#define SPP_COMING_HOME_TYPE 7
#define SPP_COMING_HOME_LENGTH 8
#define SPP_COMING_HOME_MANUAL_TIMER 9
#define SPP_COMING_HOME_MANUAL_UNLOCK_TIMER 10
#define SPP_INTENSITY_DRL_ON_TIMER 11

#define PARAM_IDX_VALUE 0
#define PARAM_IDX_DEF 1
#define PARAM_IDX_MIN 2
#define PARAM_IDX_MAX 3
#define PARAM_IDX_STEP 4
#define PARAM_IDX_IGN_TAP 5
#define PARAM_IDX_IGN_HOLD 6
#define PARAM_IDX_UNLOCK 7

#define interval 100
#define SHORT 500
#define LONG 1000

// Put here accurate measurements of your voltage divider resistor values. For example:
// Mazda 626 GF has ignSignalResistor = 9.98 and ignGndResistor = 4.91 while
// Mazda 2 DE has ignSignalResistor = 10.01 and ignGndResistor = 5.06
#define ignSignalResistor 10.01
#define ignGndResistor 5.06
// Based on the hardware voltage divider, this value will give real voltage on 12V line
#define voltageMultiplier (ignSignalResistor + ignGndResistor)/ignGndResistor

#endif