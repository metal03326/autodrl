/*
 * Function I got out if the internet (https://www.instructables.com/Secret-Arduino-Voltmeter/)
 * written by smarter people than me. I did a little bit of tweaking so I get Vcc in volts.
 */
float readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0) ;
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA, ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  double result = (high << 8) | low;

  result = 1125.3 / result; // Calculate Vcc (in V); 1125.3 = 1.1*1023
  return result; // Vcc in volts
}

int valueOf(int param) {
  return params[param][PARAM_IDX_VALUE];
}

void wakeupResistors() {
  digitalWrite(o_auto_pull_up, HIGH);
  digitalWrite(o_ldr_pull_down, LOW);
}

void sleepResistors() {
  digitalWrite(o_auto_pull_up, LOW);
  digitalWrite(o_ldr_pull_down, HIGH);
}

void resetTimers() {
  lightChangedCounter = 0;
  voltageChangedCounter = 0;
  manualComingHomeCountDown = 0;
  comingHomeRemaingTime = 0;
}

void sleep() {
  // Reset flags for redundancy
  resetTimers();
  resetServiceProcedureVariables();

  // Put resistors to sleep to minimize power consumtion
  sleepResistors();

  // Never go to sleep with any lights on
  lightsOut();

  // Wait for lights to go out
  delay(100);

  sleep_mode();
}

#if defined(DEBUG)
  void log(String message) {
    log(message, "");
  }
  void log(String message, String value) {
    Serial.print(message);
    Serial.print(" ");
    Serial.print(value);
    Serial.println();
  }
#endif

void setLights(uint8_t drl, uint8_t tns, uint8_t beams) {
  digitalWrite(o_drl, drl);
  digitalWrite(o_tns, tns);
  digitalWrite(o_beams, beams);
}

void lightsOut() {
  setLights(LOW, LOW, LOW);
}

void updateCalculatedValues() {
  int intensity = valueOf(SPP_INTENSITY);

  calculatedIntesityLowerValue = intensity - (float)intensity * valueOf(SPP_INTENSITY_HYSTERESIS) / 100;
  // If timer, this value will not be touched here (like the lower counterpart), since upper is used to figure out when engine has started, which timer will do anyhow.
  calculatedVoltageUpperValue = (float)valueOf(SPP_RUNNING_VOLTAGE) / 10;
  // If timer, do not let the module think engine shut off, ever. Ignition is the only trigger then.
  calculatedVoltageLowerValue = valueOf(SPP_RUNNING_TYPE) == RUNNING_TIMER ? 0 : calculatedVoltageUpperValue - calculatedVoltageUpperValue * valueOf(SPP_VOLTAGE_HYSTERESIS) / 100;
  calculatedIntensityDrlOnTimer = valueOf(SPP_INTENSITY_DRL_ON_TIMER) * 1000 / interval;
}

void loadParams() {
  int loadedValue = 0;

  #if defined(DEBUG)
    log("Loading parameters for version:", (String)VER);
  #endif

  // Index 0 is fictional
  for (byte i = 1; i < (sizeof(params) / sizeof(params[0])); i++) {
    // i is the beginning of a 4 byte int in the EEPROM
    EEPROM.get(i * 4, loadedValue);

    if (loadedValue < params[i][PARAM_IDX_MIN] || loadedValue > params[i][PARAM_IDX_MAX]) {
      params[i][PARAM_IDX_VALUE] = params[i][PARAM_IDX_DEF];
    } else {
      params[i][PARAM_IDX_VALUE] = loadedValue;
    }
  }

  updateCalculatedValues();

  #if defined(DEBUG)
    // Index 0 is fictional
    for (byte i = 1; i < (sizeof(params) / sizeof(params[0])); i++) {
      log("Param " + (String)i + ":", (String)valueOf(i));
    }

    log("Intensity hysteresis:", (String)calculatedIntesityLowerValue + " - " + (String)valueOf(SPP_INTENSITY));
    log("Voltage hysteresis:", (String)calculatedVoltageLowerValue + " - " + (String)calculatedVoltageUpperValue);
  #endif
}

void saveParams() {
  #if defined(DEBUG)
    log("Saving parameters...");
  #endif

  for (byte i = 1; i < (sizeof(params) / sizeof(params[0])); i++) {
    // i is the beginning of a 4 byte int in the EEPROM
    EEPROM.put(i * 4, valueOf(i));
  }

  updateCalculatedValues();
}

bool isAutoOn() {
  // LOW means ON, HIGH means OFF
  return digitalRead(i_auto) == LOW;
}
